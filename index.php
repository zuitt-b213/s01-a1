<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>S01 Activity</title>
	</head>
	<body>
		<h1>Letter-Based Grading</h1>
		<p><?php echo getLetterGrade(87) ?></p>
		<p><?php echo getLetterGrade(94) ?></p>
		<p><?php echo getLetterGrade(74) ?></p>
	</body>
</html>